{-# LANGUAGE OverloadedStrings #-}

import Blaze.ByteString.Builder.ByteString (fromByteString)
import Control.Concurrent (forkIO, MVar, newEmptyMVar, putMVar, takeMVar)
import Control.Concurrent.Chan (Chan, newChan, readChan, writeChan, dupChan)
import Control.Monad.Trans.Class (lift)
import qualified Crypto.Hash.SHA1 as SHA1
import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as C
import qualified Data.HashTable.IO as HT
import qualified Data.Text as T
import qualified Data.Text.Read as Read
import qualified Network.HTTP.Types as H
import Network.Wai (Request, Response(..), pathInfo)
import Network.Wai.Handler.Warp (run)
import System.Time (ClockTime, getClockTime)
import Text.Printf (printf)

main :: IO ()
main = do
  coreChan <- newChan
  forkIO $ coreHandler coreChan
  run 3000 $ lift . dfjService coreChan

responseBS :: H.Status
           -> H.ResponseHeaders
           -> ByteString
           -> Response
responseBS s h = ResponseBuilder s h . fromByteString

type ServiceResponse = MVar ByteString

dfjService :: Chan (CoreRequest, ServiceResponse)
           -> Request
           -> IO Response
dfjService ch request =
  case mkCoreRequest request of
    Nothing -> return $ responseBS H.badRequest400 headers "{status:\"error\"}"
    Just rq -> do putStrLn $ show rq
                  resp <- newEmptyMVar
                  writeChan ch (rq, resp)
                  body <- takeMVar resp
                  return $ responseBS H.ok200 headers body
 where headers = [("Content-Type", "application/json")]

data Plane = Plane Int Int Int deriving (Show)

type PlayerId = ByteString

data CoreRequest = SetPlanes [Plane]
                 | Reveal PlayerId Int Int
                 | GetEvent PlayerId
                 deriving (Show)

mkCoreRequest :: Request
              -> Maybe CoreRequest
mkCoreRequest req = case pathInfo req of
  ["planes", x1,y1,d1, x2,y2,d2, x3,y3,d3] -> Just $ SetPlanes [Plane (toInt x1) (toInt y1) (toInt d1),
                                                                Plane (toInt x2) (toInt y2) (toInt d2),
                                                                Plane (toInt x3) (toInt y3) (toInt d3)]
  ["reveal", id, x, y] -> Just $ Reveal (C.pack $ T.unpack id) (toInt x) (toInt y)
  ["event",  id]       -> Just $ GetEvent (C.pack $ T.unpack id)
  _                    -> Nothing
 where toInt text = case Read.decimal text of
                      Left error  -> 0
                      Right (n,_) -> n

type QueuesMap = HT.BasicHashTable PlayerId (Chan BrowserMessage)
type QueLenMap = HT.BasicHashTable PlayerId Int
type PlanesMap = HT.BasicHashTable PlayerId [Plane]
type GridsMap = HT.BasicHashTable PlayerId Grid
type TicksMap = HT.BasicHashTable PlayerId ClockTime
type PeersMap = HT.BasicHashTable PlayerId PlayerId

newQueuesMap :: IO QueuesMap
newQueuesMap = do
  ht <- HT.new
  return ht

newQueLenMap :: IO QueLenMap
newQueLenMap = do
  ht <- HT.new
  return ht

newPlanesMap :: IO PlanesMap
newPlanesMap = do
  ht <- HT.new
  return ht

newGridsMap :: IO GridsMap
newGridsMap = do
  ht <- HT.new
  return ht

newTicksMap :: IO TicksMap
newTicksMap = do
  ht <- HT.new
  return ht

newPeersMap :: IO PeersMap
newPeersMap = do
  ht <- HT.new
  return ht

coreHandler :: Chan (CoreRequest, ServiceResponse)
            -> IO ()
coreHandler ch = do
  q <- newQueuesMap
  l <- newQueLenMap
  p <- newPlanesMap
  g <- newGridsMap
  t <- newTicksMap
  r <- newPeersMap
  coreLoop ch q l p g t r Nothing 1

data CellColor = Red | Yellow | White
data Cell = Cell CellColor Bool
type Grid = [[Cell]]

data BrowserMessage = WaitForYourTurn
                    | ChooseCellToReveal
                    | YourCellRevealed Int Int
                    | PeerCellRevealed CellColor

coreLoop :: Chan (CoreRequest, ServiceResponse)
         -> QueuesMap
         -> QueLenMap
         -> PlanesMap
         -> GridsMap
         -> TicksMap
         -> PeersMap
         -> Maybe PlayerId
         -> Int
         -> IO ()
coreLoop ch queues qlens planesMap grids ticks peers waiter nextId = do
  pair <- readChan ch
  let (req, resp) = pair
  ret <- handle req waiter
  putStrLn $ "ret: " ++ show ret
  let (body, waiter') = ret
  putMVar resp body
  coreLoop ch queues qlens planesMap grids ticks peers waiter' (nextId + 1)
 where
  mkGrid pls = placePlanes g pls
    where g = grid 10 10 (Cell White False)

  grid :: Int -> Int -> a -> [[a]]
  grid x y = replicate x . replicate y

  getGrid g x y = (g !! x) !! y

  setGrid g x y c = replace g x (replace (g !! x) y c)
    where replace (_:xs) 0 x' = x':xs
          replace (x:xs) i x' = x:(replace xs (i-1) x')

  placePlanes g []      = g
  placePlanes g (p:pls) = placePlanes g' pls
    where g' = placePlane g p

  planes = [-- UP: 0
            [[0,0,2,0,0],
             [1,1,1,1,1],
             [0,0,1,0,0],
             [0,1,1,1,0],
             [0,0,0,0,0]],
            -- RIGHT: 1
            [[0,0,1,0,0],
             [1,0,1,0,0],
             [1,1,1,2,0],
             [1,0,1,0,0],
             [0,0,1,0,0]],
            -- DOWN: 2
            [[0,1,1,1,0],
             [0,0,1,0,0],
             [1,1,1,1,1],
             [0,0,2,0,0],
             [0,0,0,0,0]],
            -- LEFT: 3
            [[0,1,0,0,0],
             [0,1,0,1,0],
             [2,1,1,1,0],
             [0,1,0,1,0],
             [0,1,0,0,0]]]

  placePlane g (Plane x y d) = place g x y p 0 0
    where
      p = planes !! d

      place g x y p 5 _ = g
      place g x y p i 5 = place g  x y p (i+1) 0
      place g x y p i j = case getGrid p i j of
        0 -> place g  x y p i (j+1)
        1 -> place g' x y p i (j+1)
             where g' = setGrid g (x+i) (y+j) (Cell Yellow False)
        2 -> place g' x y p i (j+1)
             where g' = setGrid g (x+i) (y+j) (Cell Red False)

  newPlayerId :: IO PlayerId
  newPlayerId = do
    time <- getClockTime
    return $ C.pack $ hash $ (show nextId) ++ (show time)
   where
    hash = toHex . SHA1.hash . C.pack
    toHex bytes = C.unpack bytes >>= printf "%02x"

  insertPlayer id pls = do
    msgQueue <- newChan
    HT.insert queues id msgQueue
    HT.insert qlens id 0

    HT.insert planesMap id pls

    let grid = mkGrid pls
    HT.insert grids id grid

    lastTime <- getClockTime
    HT.insert ticks id lastTime

  sendMessageTo id msg = do
    res1 <- HT.lookup queues id
    case res1 of
      Nothing -> return ()
      Just ch -> do
        res2 <- HT.lookup qlens id
        case res2 of
          Nothing -> return ()
          Just ql -> do
            writeChan ch msg
            HT.insert qlens id (ql + 1)

  updateLastTime id = do
    lastTime <- getClockTime
    HT.insert ticks id lastTime

  revealCell id x y = do
    res1 <- HT.lookup grids id
    case res1 of
      Nothing -> return White
      Just g -> do
        let Cell c _ = getGrid g x y
        HT.insert grids id (setGrid g x y (Cell c True))
        return c

  handle :: CoreRequest
         -> Maybe PlayerId
         -> IO (ByteString, Maybe PlayerId)

  handle (SetPlanes pls) Nothing = do
    myId <- newPlayerId
    insertPlayer myId pls
    return (okJson myId, Just myId)

  handle (SetPlanes pls) (Just peer) = do
    myId <- newPlayerId
    insertPlayer myId pls
    HT.insert peers myId peer
    HT.insert peers peer myId
    sendMessageTo myId WaitForYourTurn
    sendMessageTo peer ChooseCellToReveal
    return (okJson myId, Nothing)

  handle (Reveal myId x y) w = do
    res1 <- HT.lookup peers myId
    case res1 of
      Nothing -> return (errorJson, w)
      Just peer -> do
        updateLastTime myId
        cell <- revealCell peer x y
        sendMessageTo peer (YourCellRevealed x y)
        sendMessageTo peer ChooseCellToReveal
        sendMessageTo myId (PeerCellRevealed cell)
        sendMessageTo myId WaitForYourTurn
        return (okJson myId, w)

  handle (GetEvent myId) w = do
    res1 <- HT.lookup queues myId
    case res1 of
      Nothing -> return (errorJson, w)
      Just ch -> do
        res2 <- HT.lookup qlens myId
        case res2 of
          Nothing -> return (errorJson, w)
          Just 0  -> return (errorJson, w)
          Just ql -> do
            HT.insert qlens myId (ql - 1)
            msg <- readChan ch
            return (C.pack $ encode msg, w)

  okJson myId = C.pack $ "{status:\"ok\",myId:" ++ show myId ++ "}"
  errorJson = "{status:\"error\"}"

  encode WaitForYourTurn = "{status:\"ok\",type:\"WaitForYourTurn\"}"
  encode ChooseCellToReveal = "{status:\"ok\",type:\"ChooseCellToReveal\"}"
  encode (YourCellRevealed x y) = "{status:\"ok\",type:\"YourCellRevealed\",x:" ++ show x ++ ",y:" ++ show y ++ "}"
  encode (PeerCellRevealed Red) = "{status:\"ok\",type:\"PeerCellRevealed\",cell:\"red\"}"
  encode (PeerCellRevealed Yellow) = "{status:\"ok\",type:\"PeerCellRevealed\",cell:\"yellow\"}"
  encode (PeerCellRevealed White) = "{status:\"ok\",type:\"PeerCellRevealed\",cell:\"white\"}"

